define(['strophe-muc', 'strophe-vcard', 'jquery', 'nunjucks', 'x2js'],
function(_0, _1, $, _2, _3) {
	"use strict";

	var exports = {};

	var nickFromJid = function(jid) {
		var nick = Strophe.getBareJidFromJid(jid);
		if (nick.indexOf('@') > -1) {
			nick = Strophe.getNodeFromJid(nick);
		}
		return nick;
	};

	var dateFormat = function(val) {
		if (val) {
			return new Date(val);
		} else {
			return new Date();
		}
	};

	var mucNick = function(val) {
		if (val) {
			return nickFromJid(Strophe.getResourceFromJid(val));
		}
		return 'Unknown';
	};

	var statusShow = function(show, online, away, busy, other) {
		if (!show || show == "chat") {
			return online;
		} else {
			if (show == "away" || show == "xa") {
				return away;
			} else {
				if (show == "dnd") {
					return busy;
				} else {
					return other;
				}
			}
		}
	};

	var sortChildElementsOf = function($container, message_selector, stamp_selector) {
		var items = $container.children(message_selector).sort((a, b) => {
			var vA = $(stamp_selector, a).text();
			var vB = $(stamp_selector, b).text();
			return (vA < vB) ? -1 : (vA > vB) ? 1 : 0;
		});
		$container.append(items);
	};

	/**
	 * rfc4122 version 4 compliant random UUID
	 */
	var randomUuid = function() {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
			var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
			return v.toString(16);
		});
	};

	exports.Apo = function(user, pass, options) {
		this._user = user;
		this._pass = pass;
		this._containers = options.containers;
		this._selectors  = options.selectors;
		this._rooms = options.rooms || [];

		// create template environment with loader
		this._nj = new nunjucks.Environment(new nunjucks.WebLoader());
		// add some custom template filters
		this._nj.addFilter('nickFromJid', nickFromJid);
		this._nj.addFilter('dateFormat', dateFormat);
		this._nj.addFilter('mucNick', mucNick);
		this._nj.addGlobal('statusShow', statusShow);
		// pre-compile all the given templates
		this._templates = Object.keys(options.template_paths).reduce(
			(tmpl, name) => {
				tmpl[name] = this._nj.getTemplate(options.template_paths[name], true);
				return tmpl;
			},
			{}
		);

		this._xmls = new XMLSerializer();
		this._x2js = new X2JS();
		this._pendingSends = {};
		this._ackSends = new Set();
		this._replayIds = new Set();

		this._multiroster = {};

		this._conn = new Strophe.Connection(
			'http://' + options.xmpp_host + ':' + options.xmpp_port + '/' + options.xmpp_url + '/'
		);
		this._conn.connect(user, pass, this.onConnect.bind(this));
	};

	exports.Apo.prototype.onConnect = function(status, reason)  {
		switch (status) {
			case Strophe.Status.CONNECTING:
				// console.log('Strophe is connecting.', reason);
				break;

			case Strophe.Status.CONNFAIL:
				this._showMessage('error', 'Failed to connect to chat server');
				break;

			case Strophe.Status.DISCONNECTING:
				console.log('Strophe is disconnecting.', reason);
				break;

			case Strophe.Status.DISCONNECTED:
				this._showMessage('warning', 'Chat server disconnected us ('+reason+')');
				console.log('Strophe is disconnected.', reason);
				break;

			case Strophe.Status.CONNECTED:
				// console.log('Strophe is connected.', reason);
				// say that we're online
				this._conn.send($pres());

				// join the muc rooms
				this._rooms.forEach(roomName => {
					this._multiroster[roomName] = {};
					this._conn.muc.join(
						roomName,
						this._user,
						(stanza) => this.onMessage(stanza),
						(stanza) => this.onPresence(stanza),
						(roster) => this.onRoster(roomName, roster)
					);
				});

				// set up input handling
				var $input = $(this._selectors.input);
				$('body').on('keypress', $input, evt => this.onInputKeypress(evt));
				$input.removeAttr('disabled').focus();
				break;

			case Strophe.Status.AUTHFAIL:
				this._showMessage('error', 'Authentication failure ('+reason+')');
				break;

			default:
				console.error('unhandled connect status', status, reason);
				break;
		}

		return true;
	};

	exports.Apo.prototype._showMessage = function(type, message) {
		// console.log(type, message, this._templates);
		this._containers.messages.append(
			this._templates[type].render({
				'message': message
			})
		);
	};

	exports.Apo.prototype.onMessage = function(stanza) {
		var json_message = this._x2js.xml2json(stanza);

		// prevent adding duplicate replayed messages to the chat window
		// this assumes that 
		if (json_message.delay) {
			if (this._replayIds.has(json_message._id)) {
				// must return true to keep this handler attached
				return true;
			}
			this._replayIds.add(json_message._id);
		}

		// if we're in multiple rooms, prevent displaying our own sent
		// messages multiple times due to receiving from each room
		if (json_message._id && json_message._id in this._pendingSends) {
			// console.log('have _pendingSends', json_message._id);
			var skipMsg = false;
			var sendId = this._pendingSends[json_message._id];
			if (this._ackSends.has(sendId)) {
				// console.log('have _ackSends', sendId);
				delete this._pendingSends[json_message._id];
				this._ackSends.delete(sendId);
				skipMsg = true;
			} else {
				// console.log('adding _ackSends', sendId);
				this._ackSends.add(sendId);
			}
			if (skipMsg) {
				// must return true to keep this handler attached
				return true;
			}
		}

		// find and re-attach HTML-IM as html string
		var html_body = stanza.querySelector('html>body>*');
		if (html_body) {
			json_message.html = this._xmls.serializeToString(html_body);
		}

		// render the message info via template and append to the chat window
		this._containers.chat.append(
			this._templates.message.render(
				json_message
			)
		);

		if (json_message.delay) {
			this._sortChatHistory();
		}

		// scroll to latest message
		this._containers.chat.scrollTop(999999999);

		// must return true to keep this handler attached
		return true;
	};

	exports.Apo.prototype.onPresence = function(stanza) {
		var json_presence = this._x2js.xml2json(stanza);

		// render the presence info via template and append to the chat window
		this._containers.chat.append(
			this._templates.presence.render(
				json_presence
			)
		);

		if (json_presence.delay) {
			this._sortChatHistory();
		}

		// scroll to latest message
		this._containers.chat.scrollTop(999999999);

		// must return true to keep this handler attached
		return true;
	};

	exports.Apo.prototype._sortChatHistory = function() {
		// sort all of the dealyed messages by their stamp,
		// since they can arrive out-of-order when we first join multiple mucs
		sortChildElementsOf(
			this._containers.chat,
			this._selectors.history_items,
			this._selectors.stamp
		);
	};

	exports.Apo.prototype.onRoster = function(room, roster) {
		var jidToNick = {};
		var vCardPromises = [];
		// for each person on the roster...
		Object.keys(roster).forEach(nick => {
			var jid = Strophe.getBareJidFromJid(roster[nick].jid);
			jidToNick[jid] = nick;
			// ... ask the service for their vCard ...
			vCardPromises.push(new Promise((resolve, reject) => {
				this._conn.vcard.get(resolve, jid, reject);
			}));
		});

		// ... wait until all the vCard requests are completed ...
		Promise.all(vCardPromises).then(vCards => {
			vCards.forEach(id => {
				// ... re-attach the vcard to the roster data ...
				var json_iq = this._x2js.xml2json(id);
				roster[jidToNick[json_iq._from]].vCard = json_iq.vCard;
			});
			return roster;
		}).then(roster => {
			// ... then render the roster info using template and update the roster container
			this._multiroster[room] = roster;
			// combine all unique users from rosters from all current mucs
			var combinedroster = Object.keys(this._multiroster).reduce((cr, roomName) => {
				Object.keys(this._multiroster[roomName]).forEach(rosterName => {
					cr[rosterName] = this._multiroster[roomName][rosterName];
				});
				return cr;
			}, {});
			this._containers.roster.html(
				this._templates.roster.render({
					'users': Object.keys(combinedroster).map(rosterName => combinedroster[rosterName])
				})
			);
		}).catch(errors => {
			console.error('vcard error', errors);
		});

		// must return true to keep this handler attached
		return true;
	};

	exports.Apo.prototype.onInputKeypress = function(evt) {
		var $input = $(this._selectors.input);
		switch (evt.keyCode) {
			case 13:
				// Enter
				var msg = $input.val();
				// keep track of sent message IDs;
				// we send messages to all group chats with the same UUID.
				// we'll use this later to prevent receiving duplicates back from the server
				// and for preventing duplicates from chat history replay from multiple rooms
				var messageId = randomUuid();
				Object.keys(this._conn.muc.rooms).forEach(roomKey => {
					var sentId = this._conn.muc.groupchat(
						roomKey, msg, null, messageId
					);
					this._pendingSends[sentId] = messageId;
				});
				$input.val('').focus();
				break;
			case 27:
				// Esc
				$input.val('').focus();
				break;
		}
	};


	return exports;
});