requirejs.config({
	baseUrl: '.',
	paths: {
		'apo': '../lib/apo',
		'text':         '../bower_components/text/text',
		'strophe':      '../bower_components/strophe/strophe.min',
		'strophe-muc':  '../bower_components/strophejs-plugins/muc/strophe.muc',
		'strophe-vcard':'../bower_components/strophejs-plugins/vcard/strophe.vcard',
		'bootstrap':    '../bower_components/bootstrap/dist/js/bootstrap.min',
		'jquery':       '../bower_components/jquery/dist/jquery.min',
		'nunjucks':     '../bower_components/nunjucks/browser/nunjucks.min',
		'x2js':         '../bower_components/x2js/xml2json.min'
	},
	shim: {
		'strophe-muc': ['strophe'],
		'strophe-vcard': ['strophe']
	}
});

requirejs([
	'jquery',
	'bootstrap',
	'apo',
	],
	function($, _, apo) {
		window.a = new apo.Apo(
			'doug@localhost/web',
			'1234',
			{
				'containers': {
					'chat': $('.chat-content'),
					'roster': $('.roster-content'),
					'messages': $('.page-header')
				},
				'selectors': {
					'input': '#message-input',
					'chat_items': 'div.message,div.presence',
					'history_items': 'div.message.delayed,div.presence.delayed,div.presence',
					'stamp': 'span.stamp'
				},
				'xmpp_host': 'localhost',
				'xmpp_port': '7070',
				'xmpp_url':  'http-bind',
				'rooms': [
					'test-room-a@conference.localhost',
					'test-room-b@conference.localhost',
				],
				'template_paths': {
					'message':  'templates/message.html',
					'roster':   'templates/roster.html',
					'presence': 'templates/presence.html',
					'error':    'templates/error.html',
					'warning':  'templates/warning.html',
				}
			}
		);
	}
);